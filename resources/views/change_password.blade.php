<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{config('app.name')}} | Forget Password</title>
    <link rel="stylesheet" type="text/css" href="{{url('login/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('login/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('login/css/iofrm-style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('login/css/iofrm-theme4.css')}}">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@700&display=swap" rel="stylesheet">

</head>
<body>
    <div class="form-body">
        <div class="website-logo" style="color:black;font-size:28px;font-family: 'Oswald', sans-serif;">
            {{config('app.name')}}
    </div>
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">
                   <a href="{{url('/')}}"> <img src="{{url('login/images/graphic1.svg')}}" alt=""> </a>
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <h3>Change your Password</h3>

                        @if($errors->has('register_success'))

                        <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $errors->first('register_success') }}
                        </div>
                        @endif
                        <form method="POST" autocomplete="off" action="{{route('change_password_action')}}">
{{ csrf_field() }}
                            <input class="form-control" type="password" name="password" placeholder="New Password" required >
                            <center class ="error">{{ $errors->first('password') }}</center>

                            <input class="form-control" type="password" name="password_confirmation" placeholder="Retype Confirmation" required >
                            <input type="hidden" name="email_token" value="{{$token}}">

                            <div class="form-button">
                                <button id="submit" type="submit" class="ibtn">Submit</button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="{{url('login/js/jquery.min.js')}}"></script>
<script src="{{url('login/js/popper.min.js')}}"></script>
<script src="{{url('login/js/bootstrap.min.js')}}"></script>
<script src="{{url('login/js/main.js')}}"></script>
</body>
</html>
