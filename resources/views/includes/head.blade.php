<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="utf-8" />
        <title>{{ config('app.name') }} | {{$title}}</title>
        <meta name="keywords" content="HTML5,CSS3,Admin Template" />
        <meta name="description" content="" />
        <meta name="Author" content="Psd2allconversion [www.psd2allconversion.com]" />

        <!-- mobile settings -->
        <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

        <!-- WEB FONTS : use %7C instead of | (pipe) -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />
        <meta name="base_url" content="{{url('/')}}">
        <!-- CORE CSS -->
        <link href="{{url('dashboard/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('dashboard/plugins/metis-menu/metisMenu.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('dashboard/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('dashboard/plugins/simple-line-icons-master/css/simple-line-icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('dashboard/plugins/animate/animate.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('dashboard/plugins/c3/c3.min.css')}}" rel="stylesheet">
        <link href="{{url('dashboard/plugins/widget/widget.css')}}" rel="stylesheet">
        <link href="{{url('dashboard/plugins/calendar/fullcalendar.min.css')}}" rel="stylesheet">
        <link href="{{url('dashboard/plugins/ui/jquery-ui.css')}}" rel="stylesheet">

        <!-- THEME CSS -->
        <link href="{{url('dashboard/css/style.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('dashboard/css/theme/dark.css')}}" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@700&display=swap" rel="stylesheet">
        <!-- PAGE LEVEL SCRIPTS -->

    </head>
