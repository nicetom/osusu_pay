@include('includes.head')
    <body class="fixed-top">

        <!-- wrapper -->
        <div id="wrapper">
            <!-- BEGIN HEADER -->
            @include('includes.header')
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->

            <!-- BEGIN CONTAINER -->
            <div class="page-container">

                @include('includes.nav')
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content-wrapper">
                        @if(count($errors) > 0)
                    @include('includes.errors')
                @endif

                    @yield('content')
                    <div class="clearfix"></div>
                   @include('includes.footer')
                </div>
            </div>
        </div>
        <!-- /wrapper -->


        <!-- SCROLL TO TOP -->
        <a href="#" id="toTop"></a>


        <!-- PRELOADER -->
        <div id="preloader">
            <div class="inner">
                <span class="loader"></span>
            </div>
        </div><!-- /PRELOADER -->

@include('includes.script')
@yield('other_script')
    </body>

</html>
