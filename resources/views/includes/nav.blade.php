<aside class="sidebar">
    <nav class="sidebar-nav">
        <ul class="metismenu" id="menu">
            <li class="active">
                <a href="{{route('home')}}"><i class="icon-grid"></i> <span class="nav-label">Home</span><span class="fa arrow"></span></a>

            </li>

            <li>
                <a href="{{route('profile')}}"><i class="fa fa-user"></i> <span class="nav-label">My Profile</span><span class="fa arrow"></span></a>

            </li>
                <li>
                <a href="{{route('bank')}}"><i class="fa fa-edit"></i> <span class="nav-label">Bank Details</span><span class="fa arrow"></span></a>

            </li>

            @if(Auth::guard('web')->user()->role == '0')
            <li>
            <a href="{{route('pair')}}"><i class="fa fa-edit"></i> <span class="nav-label">Pair Users</span><span class="fa arrow"></span></a>

            </li>

            <li>
                <a href="{{route('users')}}"><i class="fa fa-users"></i> <span class="nav-label">Users</span><span class="fa arrow"></span></a>

            </li>
            @if(Auth::guard('web')->user()->id != '3' )
            <li>
                <a href="{{route('replace_user')}}"><i class="fa fa-users"></i> <span class="nav-label">Replace Users</span><span class="fa arrow"></span></a>

            </li>
            @endif
            <li>
                <a href="{{route('statistics')}}"><i class="fa fa-users"></i> <span class="nav-label">Statistics</span><span class="fa arrow"></span></a>

            </li>

           @endif

            <li>
                <a href="{{route('logout')}}"><i class="icon-logout"></i> <span class="nav-label">Logout</span><span class="fa arrow"></span></a>

            </li>

        </ul>
    </nav>
</aside>
