<!doctype html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <title>{{ config('app.name') }} | is the easiest platform of making stress-free cash in Africa Today</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Site Description Here">
        <link href="{{url('css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{url('css/stack-interface.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{url('css/socicon.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{url('css/lightbox.min.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{url('css/flickity.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{url('css/iconsmind.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{url('css/jquery.steps.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{url('css/theme.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{url('css/custom.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{url('css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{url('css/font-rubiklato.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700%7CRubik:300,400,500" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@700&display=swap" rel="stylesheet">
        <style>
            .white{
                color: white;
                font-weight: bold;
            }
        </style>
    </head>
    <body class=" ">
        <a id="start"></a>



        <!--end of notification-->
        <div class="nav-container ">
            <div class="bar bar--sm visible-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-3 col-md-2">
                              <div class="bar__module">
                                <a href="/">
                                    African Osusu
                                </a>
                            </div>
                        </div>
                        <div class="col-9 col-md-10 text-right">
                            <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs">
                                <i class="icon icon--sm stack-interface stack-menu"></i>
                            </a>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </div>
            <!--end bar-->
            <nav id="menu1" class="bar bar--sm bar-1 hidden-xs bar--absolute">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-2 hidden-xs" style="color:black;font-size:28px;font-family: 'Oswald', sans-serif;">
                            African Osusu
                            <!--end module-->
                        </div>
                        <div class="col-lg-11 col-md-12 text-right text-left-xs text-left-sm">
                            <div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <li class="dropdown">


                                        <!--end dropdown container-->
                                    </li>
                                    <li class="dropdown">

                                        <div class="dropdown__container">
                                            <div class="container">
                                                <div class="row">

                                    </li>
                                    <li class="dropdown">

                                        <div class="dropdown__container">
                                            <div class="container">
                                                <div class="row">

                                        <!--end dropdown container-->
                                    </li>
                                    <li class="dropdown">

                                        <div class="dropdown__container">
                                            <div class="container">
                                                <div class="row">

                                    </li>
                                    <li class="dropdown">

                                        <div class="dropdown__container">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-12 dropdown__content dropdown__content--lg">
                                                        <div class="pos-absolute col-lg-5 imagebg hidden-sm hidden-xs" data-overlay="4">
                                                            <div class="background-image-holder">
                                                                <img alt="background" src="{{url('img/dropdown-2.jpg')}}" />
                                                            </div>

                                                        </div>
                                                        <div class="row justify-content-end">
                                                            <div class="col-lg-2 col-md-4">
                                                                <ul class="menu-vertical">

                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <!--end of row-->
                                                    </div>
                                                    <!--end dropdown content-->
                                                </div>
                                            </div>
                                        </div>
                                        <!--end dropdown container-->
                                    </li>
                                </ul>
                            </div>
                            <!--end module-->
                            <div class="bar__module">
                                <a class="btn btn--sm type--uppercase" href="{{route('register_page')}}">
                                    <span class="btn__text">
                                        Register
                                    </span>
                                </a>
                                @if(Auth::guard('web')->check())
                                <a class="btn btn--sm btn--primary type--uppercase" href="{{route('home')}}">
                                    <span class="btn__text">
                                        Dashboard
                                    </span>
                                </a>
                                @else
                                <a class="btn btn--sm btn--primary type--uppercase" href="{{route('login_page')}}">
                                    <span class="btn__text">
                                        Login
                                    </span>
                                </a>
                                @endif
                            </div>
                            <!--end module-->
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </nav>
            <!--end bar-->
        </div>
        <div class="main-container">
            <section class="cover unpad--bottom switchable switchable--switch bg--secondary text-center-xs">
                <div class="container">
                    <div class="row align-items-center justify-content-around">
                        <div class="col-md-6 col-lg-5 mt-0">
                            <h1>
                                {{ config('app.name') }} Now in Nigeria Get Paid Within 24 - 48 hours. Referral Bonus of ₦10,000 monthly on <b style="color:#89898a;">Silver Plan</b> only for 20 referrals.
                            </h1>

                            <a class="btn btn-primary type--uppercase" href="{{route('register_page')}}">
                                <span class="btn__text">
                                    Register
                                </span>
                            </a>
                            <span class="block type--fine-print"></span>
                            @if(Auth::guard('web')->check())
                            <a class="btn btn-success type--uppercase" href="{{route('home')}}"> <span class="btn__text">Dashboard</span> </a>
                            @else
                            <a class="btn btn-success type--uppercase" href="{{route('login_page')}}"> <span class="btn__text">Login</span> </a>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <img alt="Image" src="{{url('img/ambassador.png')}}" />
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
              <section>
			<div class="block">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="why-chooseus">
								<div class="row">
									<div class="col-md-3">
										<div class="title1 style1 leftalign">

											<h2>How Does {{ config('app.name') }} Works?</h2>
											<p>Below is a summarized step by step point on how {{ config('app.name') }} works.</p>
											<a href="#" class="theme-btn" title="">Follow the Steps</a>
										</div>
									</div>
									<div class="col-md-9">
										<div class="remove-ext2">
											<div class="row">
												<div class="col-md-4">
													<div class="chooseus-opt">


														<h2>Step 1</h2>
														<p> You must have the funds for donation before you start the registration process.</p>
													</div>
												</div>
												<div class="col-md-4">
													<div class="chooseus-opt">


														<h2>Step 2</h2>
														<p>Join {{ config('app.name') }} by registering on our official site or via a member's referral link </p>
													</div>
												</div>
												<div class="col-md-4">
													<div class="chooseus-opt">


														<h2>Step 3</h2>
														<p> The system will pair you with another member who will donate to you within 24hours.</p>
													</div>
												</div>
												<div class="col-md-4">
													<div class="chooseus-opt">


														<h2>Step 4</h2>
														<p> Notify and make arrangements with the receiving party on how to make payment and contact him/her after payment.</p>
													</div>
												</div>
												<div class="col-md-4">
													<div class="chooseus-opt">


														<h2>Step 5</h2>
														<p>Once the receiving party confirms payment, he is to acknowledge your payment on his {{ config('app.name') }} account. </p>
													</div>
												</div>
												<div class="col-md-4">
													<div class="chooseus-opt">


														<h2>Step 6</h2>
														<p> Once your payment is confirmed, you will have full access to the platform. There you can now be paired to get paid.</p>
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div><!-- Why Choose Us -->
						</div>
					</div>
				</div>
			</div>
		</section>
            <section class="text-center">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-10 col-lg-8">
                            <h2>Pricing Plans</h2>
                            <p class="lead">
                                We offer a range of plans that suit a variety of needs.
                            </p>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <section class="pricing-section-2 text-center ">
                <div class="container">
                    <div class="row">
                        @foreach ($plans as $plan)
                        <div class="col-md-12">
                                <div class="pricing pricing-3">
                                    <div class="pricing__head bg--secondary boxed">
                                        <h5 style="text-transform:uppercase;font-weight:bold;">{{$plan->name}}</h5>
                                        <span class="h1">
                                            <span class="pricing__dollar">₦</span>{{$plan->amount}}</span>
                                            You Get ₦{{$plan->amount * 2}}<br>
                                        <div class="bar__module">

                                    <a class="btn btn--sm btn--primary type--uppercase" href="{{route('register_page')}}">
                                        <span class="btn__text">
                                            Register
                                        </span>
                                    </a>
                                </div>
                                <!--end module-->
                                    </div>

                                </div>
                                <!--end pricing-->
                            </div>
                        @endforeach

                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <section class="feature-large feature-large-2 bg--secondary">
                <div class="container">
                    <div class="row justify-content-around">
                        <div class="col-md-4 col-lg-3">
                            <h3>What Is {{ config('app.name') }}?</h3>
                            <p class="lead">
                                {{ config('app.name') }} is a direct participant-to-participant payment system, It was created to ‘help’ people elevate their financial status through member-to-member donations.
                            </p>
                        </div>
                        <div class="col-md-4 col-lg-4">
                            <img alt="Image" class="border--round box-shadow-wide" src="{{url('img/landing-7.jpg')}}" />
                        </div>
                        <div class="col-md-4 col-lg-2">
                            <hr class="short" />
                            <p>
                                Participants on {{ config('app.name') }} don’t pay money to the Portals rather to whom they are being assign to provide help to.
                            </p>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <section class="cover height-80 imagebg text-center" data-overlay="6">
                <div class="background-image-holder">
                    <img alt="background" src="{{url('img/Professional+Headshot.jpg')}}" />
                </div>
                <div class="container pos-vertical-center">
                    <div class="row">
                        <div class="col-md-6">
                            <span class="h1">{{ config('app.name') }} allows you to multiply your capital the way you want it! </span>
                            <p class="lead">
                                Invite your Friends and get paid instantly...
                            </p>
                            <!--end of modal instance-->
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <section class="text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Top Countries we are in</h2>
                            <p class="lead">
                                We have been Working to stabilise your financial needs and deliver outstanding results
                            </p>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <section class="text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="list-inline list-inline--images">
                                <li>
                                    <img alt="Image" src="{{url('img/south-africa.png')}}" />
                                </li>
                                <li>
                                    <img alt="Image" src="{{url('img/nigeria.png')}}" />
                                </li>
                                <li>
                                    <img alt="Image" src="{{url('img/partner-7.png')}}" />
                                </li>
                                <li>
                                    <img alt="Image" src="{{url('img/senegal.png')}}" />
                                </li>
                                <li>
                                    <img alt="Image" src="{{url('img/togo.png')}}" />
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>

            <section style="background-color:#65689a;margin-bottom:30px;">
                <div class="container" style="padding-top:20px;">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-lg-12">
                            <div class="cool_fact_text text-center wow fadeInUp" data-wow-delay="0.4s">
                                <h2 class="white"><span class="counter">260k</span>+</h2>
                                <h5 class="white">Active Users</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="slider slider--inline-arrows" data-arrows="true">
                                <ul class="slides">
                                    <li>
                                        <div class="testimonial row justify-content-center">
                                            <div class="col-lg-2 col-md-4 col-6 text-center">
                                                <img class="testimonial__image" alt="Image" src="{{url('img/blackman.png')}}" />
                                            </div>
                                            <div class="col-lg-7 col-md-8 col-12">
                                                <span class="h3">&ldquo;This is a realistic program for anyone looking for a site to invest. Paid me regularly, keep up the good work!&rdquo;
                                                </span>
                                                <h5>Michael Owusu</h5>

                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="testimonial row justify-content-center">
                                            <div class="col-lg-2 col-md-4 col-6 text-center">
                                                <img class="testimonial__image" alt="Image" src="{{url('img/blackwoman.png')}}" />
                                            </div>
                                            <div class="col-lg-7 col-md-8 col-12">
                                                <span class="h3">&ldquo;My investment doubled in less 7 days. You should not expect anything more. Excellent customer service!&rdquo;
                                                </span>
                                                <h5>Grace Nguyen</h5>

                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="testimonial row justify-content-center">
                                            <div class="col-lg-2 col-md-4 col-6 text-center">
                                                <img class="testimonial__image" alt="Image" src="{{url('img/short.png')}}" />
                                            </div>
                                            <div class="col-lg-7 col-md-8 col-12">
                                                <span class="h3">&ldquo;My family and I want to thank you for helping us find a great opportunity. Very happy with how things are going!.&rdquo;
                                                </span>
                                                <h5>Margaret Khumalo</h5>

                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <section class="text-center imagebg" data-gradient-bg='#4876BD,#5448BD,#8F48BD,#BD48B1'>
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-lg-6">
                            <div class="cta">
                                <h2>Why not start investing Now!</h2>

                                <a class="btn btn--primary type--uppercase" href="{{route('register_page')}}">
                                    <span class="btn__text">
                                       Register
                                    </span>

                                </a>
                            </div>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <footer class="text-center-xs space--xs bg--dark ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">
                            <ul class="list-inline">
                                <li>
                                    <a href="#">
                                        <span class="h6 type--uppercase">About</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="h6 type--uppercase">Careers</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="h6 type--uppercase">Support</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-5 text-right text-center-xs">
                            <ul class="social-list list-inline list--hover">
                                <li>
                                    <a href="#">
                                        <i class="socicon socicon-google icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="socicon socicon-twitter icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="socicon socicon-facebook icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="socicon socicon-instagram icon icon--xs"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end of row-->
                    <div class="row">
                        <div class="col-md-7">
                            <span class="type--fine-print">Existing Since 2013 &copy;
                                <span class="update-year"></span> {{ config('app.name') }}</span>
                            <a class="type--fine-print" href="#">Privacy Policy</a>
                            <a class="type--fine-print" href="#">Legal</a>
                        </div>
                        <div class="col-md-5 text-right text-center-xs">
                            <a class="type--fine-print" href="#">support<?php echo '@';?>{{strtolower(str_replace(' ','',config('app.name')))}}.com</a>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </footer>
        </div>
        <!--<div class="loader"></div>-->
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
        <script src="{{url('js/jquery-3.1.1.min.js')}}"></script>
        <script src="{{url('js/flickity.min.js')}}"></script>
        <script src="{{url('js/easypiechart.min.js')}}"></script>
        <script src="{{url('js/parallax.js')}}"></script>
        <script src="{{url('js/typed.min.js')}}"></script>
        <script src="{{url('js/datepicker.js')}}"></script>
        <script src="{{url('js/isotope.min.js')}}"></script>
        <script src="{{url('js/ytplayer.min.js')}}"></script>
        <script src="{{url('js/lightbox.min.js')}}"></script>
        <script src="{{url('js/granim.min.js')}}"></script>
        <script src="{{url('js/jquery.steps.min.js')}}"></script>
        <script src="{{url('js/countdown.min.js')}}"></script>
        <script src="{{url('js/twitterfetcher.min.js')}}"></script>
        <script src="{{url('js/spectragram.min.js')}}"></script>
        <script src="{{url('js/smooth-scroll.min.js')}}"></script>
        <script src="{{url('js/scripts.js')}}"></script>
    </body>


</html>
