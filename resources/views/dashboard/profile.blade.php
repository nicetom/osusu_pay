@extends('includes.master')
@section('content')
<style>
.error{
    color:red;
}
</style>
<div class="content-wrapper container">
<h4 class="page-title">My Account</h4>
        <div class="row">
            <div class="col-md-12">
                <form class="form" method="POST" action="{{route('update_profile')}}">
                        <h5 class="page-title">Personal Information</h5>
                <div class="row">
                    {{ csrf_field() }}
                    <div class="form-row">
                        <div class="col-md-4">
                                <div class="form-group">
                                        <label for="Firstname">First-name</label>
                                        <input type="text" class="form-control" name="firstname" id="firstname" value="{{Auth::guard('web')->user()->firstname}}" >
                                    </div>
                        </div>
                        <div class="col-md-4">
                                <div class="form-group">
                                        <label for="Lastname">Last-name</label>
                                        <input type="text" class="form-control" name="lastname" id="lastname" value="{{Auth::guard('web')->user()->lastname}}" >
                                    </div>
                        </div>
                        <div class="col-md-4">
                                <div class="form-group">
                                        <label for="Email">Email</label>
                                        <input class="form-control"  value="{{Auth::guard('web')->user()->email}}" disabled>
                                    </div>
                        </div>
                        <div class="col-md-4">
                                <div class="form-group">
                                        <label for="Phone">Phone</label>
                                        <input type="text" class="form-control" name="phone"  value="{{Auth::guard('web')->user()->phone_number}}" >
                                    </div>
                        </div>
                        @if ((Auth::guard('web')->user()->pair_batch()->count() == 0) & (Auth::guard('web')->user()->pair_users()->count() == 0))
                        <div class="col-md-4">
                            <div class="form-group">
                                    <label for="Plan">Plan</label>
                                    <select name="plan" class="form-control">
                                        @foreach ($plans as $plan)
                                        @if ($plan->id == Auth::guard('web')->user()->usermetas->plan_id)
                                        <option selected value="{{$plan->id}}">{{$plan->name}}</option>
                                        @else
                                        <option value="{{$plan->id}}">{{$plan->name}}</option>
                                        @endif

                                        @endforeach
                                    </select>
                                </div>
                    </div>
                        @endif

                    </div>
                </div>
                    <br>

                <button class="btn btn-success" type="submit">Submit</button>

                </form>


            </div>


    </div>
</div>
  <script>
    function changeState(){
        document.getElementById('firstname').disabled = false;
        document.getElementById('lastname').disabled = false;
        document.getElementById('edit').style.display = 'none';
        document.getElementById('submit').style.display = 'inline';
    }
  </script>
@endsection
