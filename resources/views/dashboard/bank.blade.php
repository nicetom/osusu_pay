@extends('includes.master')

@section('content')
    <div class="content-wrapper container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title">
                        <div class="row">
                            <div class="col-sm-12">

                                <ol class="breadcrumb pull-right">
                                    <li><a href="javascript: void(0);"><i class="fa fa-home"></i></a></li>
                                    <li>Banking Information</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $bank = Auth::guard('web')->user()->usermetas->bank_id;
            $account_number = Auth::guard('web')->user()->usermetas->account_number;
                    if($bank != null && $account_number != null){
            $bank_name = Auth::guard('web')->user()->usermetas->bank->name;
                                $account_number = Auth::guard('web')->user()->usermetas->account_number;
                                if($account_number == null){
                                    $display_account_number = "Not Set";
                                }else{
                                    $display_account_number = $account_number;
                                }
                                if($bank_name == null){
                                    $display_bank = "Not Set";
                                }else{
                                    $display_bank = $bank_name;
                                }
                                }else{
                                    $display_bank = "Not Set";
                                    $display_account_number = "Not Set";

                                }?>

        <div class="table-responsive">

            <table class="table">
                <thead>
                        <tr style="background-color:#711b1b;padding:15px;color:white;">
                                <th>Bank Name</th>
                                <th>Account Number</th>
                            </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$display_bank}}</td>
                            <td>{{$display_account_number}}</td>
                        </tr>
                    </tbody>
            </table>
            </div>

            <form class="form" method="POST" action="{{route('update_bank_details')}}" autocomplete="off" >
                {{ csrf_field() }}

                    <div class="form-row">


                            <div class="col-md-6">
                                    <div class="form-group">
                                            <label for="Bank">Bank Name</label>
                                            <select type="text" class="form-control" name="bank_name" required >
                                                <option value="">Select a Bank</option>
                                                @foreach ($banks as $bank)
                                                <option value="{{$bank->id}}">{{$bank->name}}</option>

                                                @endforeach
                                            </select>
                                        </div>
                            </div>
                            <div class="col-md-6">
                                    <div class="form-group">
                                            <label for="Account Number">Account Number</label>
                                            <input type="text" class="form-control" name="account_number" value="{{old('account_number')}}" maxlength="10" required >
                                            <div class="error">{{$errors->first('account_number')}}</div>
                                        </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" id="submit" class="btn btn-primary" >Submit</button>
                        </div>


            </form>


        </div>

@endsection
