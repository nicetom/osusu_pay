@extends('includes.master')
@section('content')
<style>
.error{
    color:red;
}
</style>
<div class="content-wrapper container">
        <div class="row">
            <div class="col-md-12">
                <form class="form" method="POST" action="{{route('replace_user_action')}}">
                        <h5 class="page-title">Current Cycles</h5>
                <div class="row">
                    {{ csrf_field() }}
                    <div class="form-row">
                        <div class="col-md-6">
                                <div class="form-group">
                                        <label >Recievers</label>
                                        <select type="text" class="form-control" name="source_id" id="reciever">
                                            <option value="">Select a Reciever</option>
                                            @foreach ($pair_batches as $pair_batch)
                                                <option value="{{$pair_batch->source_id}}">{{$pair_batch->users->firstname}} {{$pair_batch->users->lastname}} - {{$pair_batch->users->email}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                    <label>Payers</label>
                                    <select type="text" class="form-control" id="payer" name="payer_id">

                                    </select>
                                </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                                <label>Unpaired Users</label>
                                <select type="text" class="form-control" id="free_users" name="free">

                                </select>
                            </div>
                </div>



                    </div>
                </div>
                    <br>

                <button class="btn btn-success" type="submit">Submit</button>

                </form>


            </div>


    </div>
</div>

@endsection
