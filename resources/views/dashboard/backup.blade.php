@extends('includes.master')

@section('content')
    <div class="content-wrapper container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title">
                        <div class="row">
                            <div class="col-sm-12">
                                    @if ((Auth::guard('web')->user()->usermetas->bank_id == null) && (Auth::guard('web')->user()->usermetas->account_number == null) )
                                        <div class="alert alert-danger">
                                            <p><h4 style="padding: 5px;">Welcome! {{Auth::guard('web')->user()->firstname}}, {{Auth::guard('web')->user()->lastname}}. Please Complete your bank information Now, so you can be Paired to be Paid.</h4></p>
                                        </div>

                                    @endif


                                <ol class="breadcrumb pull-right">
                                    <li><a href="javascript: void(0);"><i class="fa fa-home"></i></a></li>
                                    <li>Dashboard</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end .page title-->

            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="card cyan white-text clearfix">

                        <div class="card-content clearfix">
                            <i class="icon-settings background-icon"></i>
                            <p class="card-stats-title right panel-title wdt-lable">Plan: {{Auth::guard('web')->user()->usermetas->plan->name}}</p><br>
                            <h4 class="right panel-middle margin-b-0 wdt-lable">₦
                                {{Auth::guard('web')->user()->usermetas->plan->amount}}
                            </h4>

                            <div class="clearfix"></div>
                        </div>

                    </div>
                </div>


                <div class="col-md-6 col-lg-3">
                    <div class="card teal white-text clearfix">

                        <div class="card-content clearfix">
                            <i class="icon-user background-icon"></i>
                            <p class="card-stats-title right panel-title wdt-lable">Referrals: Registered</p><br>
                            <h4 class="right panel-middle margin-b-0 wdt-lable">{{Auth::guard('web')->user()->referrals->referral_registered}}</h4>

                            <div class="clearfix"></div>
                        </div>

                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                        <div class="card teal white-text clearfix">

                            <div class="card-content clearfix">
                                <i class="icon-user background-icon"></i>
                                <p class="card-stats-title right panel-title wdt-lable">Referrals: clicks</p><br>
                                <h4 class="right panel-middle margin-b-0 wdt-lable">{{Auth::guard('web')->user()->referrals->referral_clicks}}</h4>

                                <div class="clearfix"></div>
                            </div>

                        </div>
                    </div>


            </div>







            <div class="row">
                <div class="col-md-12">
                    <label for="" class="col-md-3">Referral Link</label>
                    <input type="text" class="form-control"  value="{{route('referral',Auth::guard('web')->user()->referrals->tag)}}">
                </div>

                <div class="col-md-12">

                    @if (isset($donatee))
                    <div class="panel panel-card recent-activites">
                    <!-- Start .panel -->
                    <div class="panel-heading">
                       You are to Donate HELP To
<?php $i = 1; ?>
                    </div>
                    <div class="panel-body text-center">
                        <div class="table-responsive table-commerce">
                            <table id="basic-datatables" class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="w80">
                                            <strong>ID</strong>
                                        </th>
                                        <th>
                                            <strong>FULLNAME</strong>
                                        </th>
                                        <th>
                                            <strong>BANKNAME</strong>
                                        </th>
                                        <th>
                                            <strong>ACCOUNTNO</strong>
                                        </th>
                                        <th>
                                            <strong>AMOUNT</strong>
                                        </th>
                                        <th>
                                            <strong>PHONENO</strong>
                                        </th>
                                        <th class="text-center">
                                            <strong>TRANSACTION STATUS</strong>
                                        </th>

                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$donatee->pair_batch->users->firstname}} {{$donatee->pair_batch->users->lastname}} </td>
                                            <td>{{$donatee->pair_batch->users->usermetas->bank->name}} </td>
                                            <td>{{$donatee->pair_batch->users->usermetas->account_number}} </td>
                                            <td>{{$donatee->pair_batch->users->usermetas->plan->amount}} </td>
                                            <td>{{$donatee->pair_batch->users->phone_number}} </td>
                                            @if ($donatee->paid == '1')
                                            <td> <span class="badge" style="background-color:green;color:black;">Success</span> </td>
                                            @else
                                            <td> <span class="badge" style="background-color:yellow;color:black;">Pending</span> </td>
                                            @endif
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- End .panel -->
                    @endif
                    @if (!isset($donators) && !isset($donatee))
                        <div style="background-color:#5acc5a;color:#333333;padding:10px;text-align:center;">Please Wait a while, You'll be paired shortly
                            @if (Auth::guard('web')->user()->pair_batch->count() > 0)
                            @if (Auth::guard('web')->user()->active != '0')
                            <a href="{{route('upgradePage')}}" class="btn btn-success">Upgrade</a>
                            @if (Auth::guard('web')->user()->usermetas->plan_id > 1)
                            <a href="{{route('recycle')}}" class="btn btn-primary">Recycle Plan</a>
                            @endif

                            @endif

                            @endif
                        </div>
                    @endif
                    @if (isset($donators))
                    <div class="panel panel-card recent-activites">
                        <!-- Start .panel -->
                        <div class="panel-heading">
                           You are to Expecting donations from
<?php $i = 1; ?>
                        </div>
                        <div class="panel-body text-center">
                            <div class="table-responsive table-commerce">
                                <table id="basic-datatables" class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th class="w80">
                                                <strong></strong>
                                            </th>
                                            <th>
                                                <strong>FULLNAME</strong>
                                            </th>
                                            <th>
                                                <strong>BANKNAME</strong>
                                            </th>
                                            <th>
                                                <strong>ACCOUNTNO</strong>
                                            </th>
                                            <th>
                                                <strong>AMOUNT</strong>
                                            </th>
                                            <th>
                                                <strong>PHONENO</strong>
                                            </th>
                                            <th class="text-center">
                                                <strong>TRANSACTION STATUS</strong>
                                            </th>
                                            <th class="text-center">
                                                    <strong>Action</strong>
                                                </th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($donators as $donator)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$donator->firstname}} {{$donator->lastname}}</td>
                                                <td>{{$donator->usermetas->bank->name}}</td>
                                                <td>{{$donator->usermetas->account_number}}</td>
                                                <td>{{$donator->usermetas->plan->amount}}</td>
                                                <td>{{$donator->phone_number}}</td>
                                                @if ($donator->onepair->paid == '1')
                                            <td> <span class="badge" style="background-color:green;color:black;" >Success</span> </td>
                                            @else
                                            <td> <span class="badge" style="background-color:yellow;color:black;">Pending</span> </td>
                                            @endif

                                            @if ($donator->onepair->paid == '0')
                                            <td> <form method="POST" action="{{route('confirm')}}" >
                                                @csrf
                                                <input name="pair_id" type="hidden" value="{{$donator->onepair->id}}"/>
                                                <button type="submit" class="btn btn-success btn-sm">Confirm Payment</button>
                                                </form> </td>
                                            @endif

                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @endif


                </div>
            </div>




        </div>
@endsection
