@extends('includes.master')
@section('content')
<div class="content-wrapper container">

    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Plan</th>
                    <th>Type</th>
                    <th>Action</th>
                    <th>Completed Details</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                <tr>
                    <td>{{$user->firstname}}</td>
                    <td>{{$user->lastname}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone_number}}</td>
                    <td>{{$user->usermetas->plan->name}} - ₦{{$user->usermetas->plan->amount}}</td>
                    @if ($user->role == '0')
                    <td>Administrator</td>
                    @else
                    <td>Normal User</td>
                    @endif

                    @if (($user->role == '0') | ($user->active == '3') )
                    <td></td>
                    @else
                    <td> <form action="{{route('ban_user',$user->id)}}" method="POST">
                        {{ csrf_field() }}
                        <button class="btn btn-danger" >Ban</button>
                    </form> </td>
                    @endif
                    @if ($user->usermetas()->where('bank_id','!=',null)->where('account_number','!=',null)->count() == '1')
                    <td > <span class="label label-success">Completed</span> </td>
                    @else
                    <td > <span class="label label-danger">Not Completed</span></td>

                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
