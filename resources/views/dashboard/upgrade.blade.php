@extends('includes.master')
@section('content')
<style>
.error{
    color:red;
}
</style>
<div class="content-wrapper container">
<h4 class="page-title">My Account</h4>
        <div class="row">
            <div class="col-md-12">
                <form class="form" method="POST" action="{{route('upgrade_plan')}}">
                        <h5 class="page-title">Upgrade Plan</h5>
                <div class="row">
                    {{ csrf_field() }}

                        <div class="col-md-6">
                                <div class="form-group">
                                        <label for="Plan">Current Plan</label>
                                        <input type="text" class="form-control" value ="{{Auth::guard('web')->user()->usermetas->plan->name}} - ₦{{Auth::guard('web')->user()->usermetas->plan->amount}}"disabled>
                                    </div>
                        </div>

                        <div class="col-md-6">
                                <div class="form-group">
                                        <label for="Plan">New Plan</label>
                                        <select name="new_plan" class="form-control">
                                            @foreach ($plans as $plan)
                                            @unless($plan->id == Auth::guard('web')->user()->usermetas->plan->id)
                                            <option value="{{$plan->id}}">{{$plan->name}} - ₦{{$plan->amount}}</option>
                                            @endunless

                                            @endforeach
                                        </select>
                                    </div>
                        </div>
                    </div>
                </div>
                    <br>

<div class="col-md-12">
    <button type="submit" id="submit" name="submit" class="btn btn-primary" >Save Changes</button>
</div>

                </form>


            </div>


    </div>
</div>

@endsection
