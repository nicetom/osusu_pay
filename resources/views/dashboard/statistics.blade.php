@extends('includes.master')

@section('content')
    <div class="content-wrapper container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title">
                        <div class="row">
                            <div class="col-sm-12">

                                <ol class="breadcrumb pull-right">
                                    <li><a href="javascript: void(0);"><i class="fa fa-home"></i></a></li>
                                    <li>Statistics</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead style="background-color:black;color:white;">
                        <tr>
                            <th>Plan Name</th>
                            <th>Amount</th>
                            <th>Number of Users</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($plans as $plan)
                            <tr>
                                <td>{{$plan->name}}</td>
                                <td>{{$plan->amount}}</td>
                                <td>{{$plan->usermetas()->count()}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
<br>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead style="background-color:black;color:white;">
                        <tr>
                            <th>Total User</th>
                            <th>Completed Profile</th>
                            <th>Uncompleted Profile</th>
                            <th>Currently Paired Users</th>
                            <th>Users Waiting to get Paired</th>
                            <th>Currently Paying Users</th>
                            <th>Users Waiting to Pay for Bronze</th>
                            <th>Users Waiting to Pay for Silver</th>

                        </tr>
                    </thead>
                    <tr>
                        <td> {{$total}} </td>
                        <td>{{$profile_complete}}</td>
                        <td>{{$profile_not_complete}}</td>
                        <td>{{$paired}}</td>
                        <td>{{$waiting_user}}</td>
                        <td>{{$current_paying}}</td>
                        <td>{{$bronze_pay}}</td>
                        <td>{{$silver_pay}}</td>
                        
                    </tr>
                    <tbody>

                    </tbody>
                </table>
            </div>

        </div>
@endsection
