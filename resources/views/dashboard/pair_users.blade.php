@extends('includes.master')

@section('content')
<link href="{{url('dashboard/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet">
<style>
.form-group .bootstrap-select.btn-group, .form-horizontal .bootstrap-select.btn-group, .form-inline .bootstrap-select.btn-group {
    border: 1px #d2cbcb solid;
}
</style>
<div class="content-wrapper container">
    <h4 class="page-title">My Account</h4>
            <div class="row">
                <div class="col-md-12">
                    <form class="form" method="POST" action="{{route('pair_action')}}">
                            <h5 class="page-title">Pair User</h5>
                    <div class="row">
                        {{ csrf_field() }}
                        <div class="form-row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                            <label for="User">User</label>
                                            <select name="source" class="form-control" id="user">
                                                @foreach($users as $user)
                                                <option value="{{$user->id}}">{{$user->firstname}} {{$user->lastname}} - {{$user->usermetas->plan->name}} ₦{{$user->usermetas->plan->amount}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                            </div>
                            <div class="col-md-6">
                                    <div class="form-group">
                                            <label for="Lastname">User to be Paired</label>
                                            <select name="users[]" class="form-control" required multiple id="parent">

                                            </select>
                                        </div>
                            </div>
                        </div>
                    </div>
                        <br>

                    <button type="submit" name="submit" class="btn btn-primary" >Pair</button>

                    </form>


                </div>


        </div>
    </div>
    @section('other_script')
    <script src="{{url('js/ajax.js')}}" ></script>
    <script>
        function getUsers(user_id){
            let data = get_users_by_plan(user_id);
            processData(data,'parent');

        }
    </script>
    @endsection

@endsection
