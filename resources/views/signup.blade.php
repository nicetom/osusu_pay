<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{config('app.name')}} - Sign up</title>
    <link rel="stylesheet" type="text/css" href="{{url('login/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('login/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('login/css/iofrm-style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('login/css/iofrm-theme4.css')}}">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@700&display=swap" rel="stylesheet">

</head>
<body>
    <div class="form-body">
        <div class="website-logo" style="color:black;font-size:28px;font-family: 'Oswald', sans-serif;">
            {{config('app.name')}}
        </div>
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">
                    <a href="{{url('/')}}"> <img src="{{url('login/images/graphic1.svg')}}" alt=""> </a>
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <h3>Double Your Investment the way you want it</h3>
                        <p>With {{config('app.name')}}</p>
                        <div class="page-links">
                            <a href="{{route('login_page')}}">Login</a><a href="{{route('register_page')}}" class="active">Register</a>
                        </div>
                        <form method="POST" autocomplete="off" action="{{route('registerUser')}}" >
{{ csrf_field() }}
                            <input class="form-control" type="text" name="first_name" placeholder="First Name" required value="{{old('first_name')}}">
                            <center class ="error">{{ $errors->first('first_name') }}</center>
                            <input class="form-control" type="text" name="last_name" placeholder="Last Name" required value="{{old('last_name')}}">
                            <center class ="error">{{ $errors->first('last_name') }}</center>
                             <input class="form-control" type="text" name="phone" placeholder="Phone Number" required value="{{old('phone')}}">
                            <center class ="error">{{ $errors->first('phone') }}</center>
                            <input class="form-control" type="email" name="email" placeholder="E-mail Address" required value="{{old('email')}}">
                            <center class ="error">{{ $errors->first('email') }}</center>

                            <input class="form-control" type="password" name="password" placeholder="Password" minLength='8' required>

                            <input class="form-control" type="password" name="password_confirmation" placeholder="Confirm Password" minLength='8' required>
                            <center class ="error">{{ $errors->first('password') }}</center>
                            <input class="form-control" type="text" name="referral" placeholder="Referral Code (Optional)" value={{$referral}}>
                            <center class ="error">{{ $errors->first('referral') }}</center>

                            <select required  name="plan_id" class="form-control" >
                                <option value="">---Select a Package---</option>
                                @foreach ($plans as $plan)
                                <option value="{{$plan->id}}">{{$plan->name}} - &#8358; {{$plan->amount}} NGN</option>
                                @endforeach
                            </select>
                            <br>

                            <div class="form-button">
                                <button id="submit" type="submit" class="ibtn">Register</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="{{url('login/js/jquery.min.js')}}"></script>
<script src="{{url('login/js/popper.min.js')}}"></script>
<script src="{{url('login/js/bootstrap.min.js')}}"></script>
<script src="{{url('login/js/main.js')}}"></script>
</body>
</html>
