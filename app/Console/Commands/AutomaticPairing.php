<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\Pair_Batch;
use App\Models\Pair_Users;
use Illuminate\Console\Command;

class AutomaticPairing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'automatic:pairing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatically Pair Users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::where('active','1')->doesntHave('pair_batch')->get();

        foreach($users as $user){

            $users_to_pair = User::where('active','0')->whereHas('usermetas', function($query) use($user){
                $query->where('plan_id', $user->usermetas->plan_id)->where('bank_id','!=',null)->where('account_number','!=',null);
            })->doesntHave('pair_user')->take(4)->get();

                if($users_to_pair->count() == 4){
                    $new_batch = new Pair_Batch;
                    $new_batch->source_id = $user->id;
                    $new_batch->active = '1';
                    $new_batch->save();

        foreach($users_to_pair as $user1){
            $pair_users = new Pair_Users;
            $pair_users->batch_id = $new_batch->id;
            $pair_users->reciever_id = $user1->id;
            $pair_users->paid = '0';
            $pair_users->plan_id = User::find($user1->id)->usermetas->plan->id;
            $pair_users->save();
        }
                }
        }
        $this->automaticPair2();
    }

    public function automaticPair2(){
        $users = User::where('active','1')->whereHas('pair_batch', function($query){
            $query->where('active','0');
        })->get();

        foreach($users as $user){
            $users_to_pair = User::where('active','0')->whereHas('usermetas', function($query) use($user){
                $query->where('plan_id', $user->usermetas->plan_id)->where('bank_id','!=',null)->where('account_number','!=',null);
            })->take(4)->get();

                if($users_to_pair->count() == 4){
                    $new_batch = new Pair_Batch;
                    $new_batch->source_id = $user->id;
                    $new_batch->active = '1';
                    $new_batch->save();

        foreach($users_to_pair as $user1){
            $pair_users = new Pair_Users;
            $pair_users->batch_id = $new_batch->id;
            $pair_users->reciever_id = $user1->id;
            $pair_users->paid = '0';
            $pair_users->plan_id = User::find($user1->id)->usermetas->plan->id;
            $pair_users->save();
        }
                }
        }
    }
}
