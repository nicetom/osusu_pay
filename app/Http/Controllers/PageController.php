<?php

namespace App\Http\Controllers;
use App\Models\Plans;
use App\Models\PasswordReset;
use App\Models\Referal;
use Auth;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home(Request $request){
        $plans = $this->getPlans();
        return view('index',['plans' => $plans]);
    }

    public function loginPage(Request $request){
        // if(Auth::guard('web')->check()){
        //     return redirect()->route('home');
        // }else{
        //     return view('signin');
        // }

        return view('signin');

    }

    public function registerPage(Request $request){
        $plans = $this->getPlans();
        return view('signup',['plans' => $plans,'referral' => ""]);
    }

    public function getPlans(){
        $plans = Plans::all();
        return $plans;
    }

    public function referralLink(Request $request,$tag){
        $ref = Referal::where('tag',$tag)->count();
        if($ref == 0){
            return "Wrong Referral Code";
        }else{
            $ref = Referal::where('tag',$tag)->increment('referral_clicks',1);
            return view('signup',['plans' => $this->getPlans(),'referral' => $tag]);
        }
    }

    public function forgetPassword(Request $request){
        return view('forget');
    }

    public function changePassword(Request $request,$token){
        $token_check = PasswordReset::where('token',$token)->first();

            if(empty($token) || empty($token_check) || ($token_check->status == '1')){
                return redirect()->route('forget')->withErrors(['token_error' => 'Invalid Password Reset Link']);
            }else{
                return view('change_password',['token'=>$token]);
            }
        return view('change_password');
    }



}
