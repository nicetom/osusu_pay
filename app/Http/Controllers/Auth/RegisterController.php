<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/signup';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'max:255', 'alpha'],
            'last_name' => ['required', 'alpha', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required','numeric','min:11','unique:users,phone_number'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'password_confirmation' => ['required'],
            'plan_id' => ['required','exists:plans,id','integer']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function createUser(Request $request)
    {
        $validation = $this->validator($request->except('_token'));

        if($validation->fails()){
            return redirect()->back()->withErrors($validation->getMessageBag())->withInput();
        }else{
            $user_create =  User::create([
                'firstname' => ucfirst(strtolower($request['first_name'])),
                'lastname' => ucfirst(strtolower($request['last_name'])),
                'email' => $request['email'],
                'phone_number' => $request['phone'],
                'password' => Hash::make($request['password']),
            ]);

            if($user_create){
                $user_create->usermetas()->create(['user_id'=>$user_create->id,'plan_id' => $request['plan_id']]);
            }

            return redirect()->route('login_page')->withErrors(["register_success" => "Your account has been created successfully"]);
        }

    }
}
