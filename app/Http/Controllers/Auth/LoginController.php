<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Auth;
use Session;

class LoginController extends Controller
{


    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function validateData(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'exists:users'],
            'password' => ['required', 'min:8']
        ]);
    }

    public function authenticate(Request $request)
    {
        $validation = $this->validateData($request->except('_token'));

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->getMessageBag())->withInput();
        } else {

            if (Auth::guard('web')->attempt(['email' => $request['email'], 'password' => $request['password'], 'active' => '3'], 1)) {
                $this->logout($request);
                Session::put('red', 1);
                return redirect()->route('login_page')->withErrors(['register_success' => "Your Account has been Banned"]);
            }else if(Auth::guard('web')->attempt(['email' => $request['email'], 'password' => $request['password']], 1)) {
                Session::put('green', 1);
                return redirect()->route('home')->withErrors(['welcome_message' => 'Welcome ' . Auth::guard('web')->user()->firstname . " " . Auth::guard('web')->user()->lastname]);
            } else {
                return redirect()->back()->withErrors(['password' => 'Password is incorrect'])->withInput();
            }
        }
    }

    public function logout(Request $request)
    {
        Auth::guard('web')->logout();
        return redirect()->route('login_page')->withErrors(['register_success' => 'You have been logged out successfully']);
    }
}
