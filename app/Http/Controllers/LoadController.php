<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\PasswordReset;
use App\Models\UserMeta;
use App\Models\Referal;
use App\Models\Pair_Batch;
use App\Models\Pair_Batch_Backup;
use App\Models\Pair_Users;
use App\Models\QueueMatch;
use App\Models\QueuePair;
use App\Models\Pair_User_Backup;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Mail\ForgetPassword;
use Str;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class LoadController extends Controller
{
    public function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'max:255', 'string'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required','numeric','min:11'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'password_confirmation' => ['required'],
            'plan_id' => ['required','exists:plans,id','integer'],
            'referral' => ['nullable','exists:referrals,tag']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function createUser(Request $request)
    {
        $validation = $this->validator($request->except('_token'));

        if($validation->fails()){
            return redirect()->back()->withErrors($validation->getMessageBag())->withInput();
        }else{
            $user_create =  User::create([
                'firstname' => ucfirst(strtolower($request['first_name'])),
                'lastname' => ucfirst(strtolower($request['last_name'])),
                'email' => $request['email'],
                'phone_number' => $request['phone'],
                'password' => Hash::make($request['password']),
                'active' => '0'
            ]);

            if(!empty($request['referral'])){
                $reff2 = Referal::where('tag',$request['referral'])->increment('referral_registered',1);
            }

            if($user_create){
                $user_create->usermetas()->create(['user_id'=>$user_create->id,'plan_id' => $request['plan_id']]);
                $referal = new Referal;
                $referal->user_id = $user_create->id;
                $referal->tag = md5($user_create->id);
                $referal->save();

                $user_create->queue_pair()->create([
                    'user_id' => $user_create->id,
                ]);
                $this->newPairing($request);
            }

            return redirect()->route('login_page')->withErrors(["register_success" => "Your account has been created successfully"]);
        }

    }

    public function newPairing(Request $request){
        $queue_matches = QueueMatch::where('status','0')->whereHas('user',function($query){
            $query->where('active','1')->whereHas('usermetas', function($query1){
                $query1->where('bank_id','!=',null)->where('account_number','!=',null);
            });
        })->get();


            foreach($queue_matches as $match){
                $pairs = QueuePair::where('status','0')->whereHas('user', function($query)use($match){
                    $query->whereHas('usermetas', function($query1) use($match){
                        $query1->where('bank_id','!=',null)->where('account_number','!=',null)->where('plan_id',$match->user->usermetas->plan_id);
                    });
                })->take(2)->get();

                    if($pairs->count() == 2){
                        $batch = Pair_Batch::create([
                            'source_id' => $match->user_id,
                            'active' => '1',
                        ]);
                            foreach($pairs as $pair){
                                $batch->pair_users()->create([
                                    'reciever_id' => $pair->user_id,
                                    'paid' => '0',
                                    'plan_id' => $batch->users->usermetas->plan_id
                                ]);
                                    $pair->status = '1';
                                    $pair->save();
                            }
                            $match->status = '1';
                            $match->save();
                    }

            }
    }

    public function getPayers(Request $request,$source_id){
        $batch = Pair_Batch::where('source_id',$source_id)->first();
        $payers = $batch->pair_users;
        $payers_array = array();
            foreach($payers as $payer){
                $payers_array[$payer->id] = $payer->user->firstname.' '.$payer->user->lastname.' - '.$payer->user->email;
            }
        return response()->json($payers_array);
    }

    public function getUnpairedUsers(Request $request,$source_id){
        $us = User::find($source_id);
        $plan_id = $us->usermetas->plan_id;
        $users_to_return = QueuePair::where('status','0')->whereHas('user', function($query)use($plan_id){
            $query->whereHas('usermetas', function($query1) use($plan_id){
                $query1->where('bank_id','!=',null)->where('account_number','!=',null)->where('plan_id',$plan_id);
            });
        })->get();


        $data = array();
        foreach($users_to_return as $pair){

            $data[$pair->user->id] = $pair->user->firstname." ".$pair->user->lastname." - ".$pair->user->usermetas->plan->name." ".$pair->user->usermetas->plan->amount;
        }

        // $users = User::where('active','0')->doesntHave('pair_users')->whereHas('usermetas', function($query) use($plan_id){
        //     $query->where('plan_id',$plan_id)->where('bank_id','!=',null)->where('account_number','!=',null);
        // })->get();
        // $users_array = array();

        //     foreach($users as $user){
        //             $users_array[$user->id] = $user->firstname.' '.$user->lastname.' - '.$user->email;
        //     }



        return response()->json($data);
    }

    public function forgetPassword(Request $request){
        $data = $request->except('_token');
        $rules = [
            'email' => ['required','exists:users']
        ];
        $messages = [
            'email.exists' => 'This Email is Invalid',
        ];

        $validation = Validator::make($data,$rules,$messages);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation->getMessageBag())->withInput();
        }else{
            $token = Str::random(100);
            $email_check = PasswordReset::where('email',$data['email'])->first();
                if(empty($email_check)){
                    $pass_reset = PasswordReset::create([
                        'email' => $data['email'],
                        'token' => $token,
                        'status' => '0'
                    ]);
                }else{
                    $email_check->token = $token;
                    $email_check->save();
             }


            $link = route('change_password',[$token]);
            Mail::to($data['email'])->send(new ForgetPassword($link));


            return redirect()->route('login_page')->withErrors(["register_success" => "Go to your Email to Reset your Password"]);

        }

    }

    public function changePassword(Request $request){
        $data = $request->except('_token');
        $rules = [
            'password' => 'required|min:8|max:25|confirmed',
            'password_confirmation' => 'required'
        ];

        $validation = Validator::make($data,$rules);

        if($validation->fails()){

            return redirect()->back()->withErrors($validation->getMessageBag())->withInput();
        }else{
            $token = PasswordReset::where('token',$data['email_token'])->first();

            $password_change_success = User::where('email',$token->email)->update([
                'password' => Hash::make($data['password'])
                ]);
                $token->status = '1';
                $token->save();

        return redirect()->route('login_page')->withErrors(['register_success' => 'Your Password has been changed Successfully']);

        }

    }

    public function automaticPairing(Request $request){
    $this->automaticPairing1($request);
       $users = User::where('active','1')->doesntHave('pair_batch')->get();

       foreach($users as $user){
        $users_to_pair = User::where('active','0')->doesnthave('pair_users')->whereHas('usermetas', function($query) use($user){
            $query->where('plan_id', $user->usermetas->plan_id)->where('bank_id','!=',null)->where('account_number','!=',null);
        })->take(2)->get();


            if($users_to_pair->count() == 2){
                $new_batch = Pair_Batch::create([
                    'source_id' => $user->id,
                    'active' => '1'
                ]);

                Pair_Batch_Backup::create([
                    'source_id' => $user->id,
                    'active' => '1'
                ]);

            foreach($users_to_pair as $user1){
                $pair_users = Pair_Users::create([
                    'batch_id' => $new_batch->id,
                    'reciever_id' => $user1->id,
                    'paid' => '0',
                    'plan_id' => User::find($user1->id)->usermetas->plan->id
                ]);

                Pair_User_Backup::create([
                    'batch_id' => $new_batch->id,
                    'reciever_id' => $user1->id,
                    'paid' => '0',
                    'plan_id' => User::find($user1->id)->usermetas->plan->id
                ]);

            }
                    }
            }

    }

    public function populateQueue(Request $request){
        $users = User::where('active','0')->doesntHave('pair_users')->get();
        $users1 = User::where('active','0')->has('pair_users')->get();
        $users2 = User::where('active','1')->doesntHave('pair_batch')->get();
        $users3 = User::where('active','1')->has('pair_batch')->get();

            //dd($users,$users1,$users2,$users3);
            foreach($users as $user){
                QueuePair::create([
                    'user_id' => $user->id
                ]);
            }

            foreach($users1 as $user){
                QueuePair::create([
                    'user_id' => $user->id,
                    'status' => '1'
                ]);
            }

            foreach($users2 as $user){
                QueueMatch::create([
                    'user_id' => $user->id
                ]);
            }

            foreach($users3 as $user){
                QueueMatch::create([
                    'user_id' => $user->id,
                    'status' => '1'
                ]);
            }

    }

    public function automaticPairing1(Request $request){

       $users = User::where('active','1')->doesntHave('pair_batch_backup')->get();

       foreach($users as $user){
        $users_to_pair = User::where('active','0')->doesnthave('pair_users_backup')->whereHas('usermetas', function($query) use($user){
            $query->where('plan_id', $user->usermetas->plan_id)->where('bank_id','!=',null)->where('account_number','!=',null);
        })->take(2)->get();


            if($users_to_pair->count() == 2){
                $new_batch = Pair_Batch::create([
                    'source_id' => $user->id,
                    'active' => '1'
                ]);

                Pair_Batch_Backup::create([
                    'source_id' => $user->id,
                    'active' => '1'
                ]);

            foreach($users_to_pair as $user1){
                $pair_users = Pair_Users::create([
                    'batch_id' => $new_batch->id,
                    'reciever_id' => $user1->id,
                    'paid' => '0',
                    'plan_id' => User::find($user1->id)->usermetas->plan->id
                ]);

                Pair_User_Backup::create([
                    'batch_id' => $new_batch->id,
                    'reciever_id' => $user1->id,
                    'paid' => '0',
                    'plan_id' => User::find($user1->id)->usermetas->plan->id
                ]);

            }
                    }
            }
    }
}
