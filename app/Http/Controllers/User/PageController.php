<?php

namespace App\Http\Controllers\User;
use App\Models\Banks;
use App\Models\User;
use App\Models\QueuePair;
use App\Models\Pair_Batch;
use App\Models\Pair_Users;
use App\Models\Plans;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;


class PageController extends Controller
{
    public function indexPage(Request $request){
            $title = "Home";

            if(Auth::guard('web')->user()->active == '0'){

                if(Auth::guard('web')->user()->pair_users()->where('paid','0')->count() > 0){
                    $donatee = Auth::guard('web')->user()->pair_users()->where('paid','0')->first();
                    return view('dashboard.home',['title' => $title,'donatee' => $donatee]);
                }else{
                    return view('dashboard.home',['title' => $title]);
                }

            }else if(Auth::guard('web')->user()->active == '1'){

                if(Auth::guard('web')->user()->pair_batch()->where('active','1')->count() > 0){
                    $donators = Auth::guard('web')->user()->pair_batch()->where('active','1')->first();
                    return view('dashboard.home',['title' => $title,'donators' => $donators->pair_users]);
                }else{
                    return view('dashboard.home',['title' => $title]);
                }
            }else{
                return view('dashboard.home',['title' => $title]);
            }


    }

    public function indexPage2(Request $request){
        $title = "Home";
        if(Auth::guard('web')->user()->active == '0'){
            $donatee = Pair_Users::where('reciever_id',Auth::guard('web')->user()->id)->first();
            return view('dashboard.home',['title' => $title,'donatee' => $donatee]);
        }else{
        $active_batch = User::whereHas('pair_batch', function($query){
            $query->where('active','1')->where('source_id',Auth::guard('web')->user()->id);
        })->first('id');

        if($active_batch == null){
            return view('dashboard.home',['title' => $title]);
        }else{
            $donators = User::whereHas('pair_users', function($query) use($active_batch){
                $query->where('batch_id',$active_batch->id);
            })->get();
            return view('dashboard.home',['title' => $title,'donators' => $donators]);
        }

    }


    }



    public function replaceUsers(Request $request){
        $title = "Replace Users";
        $pair_batches = Pair_Batch::where('active','1')->get();

        return view('dashboard.replace',['title' => $title,'pair_batches'=>$pair_batches]);
    }

    public function profilePage(Request $request){
        $title = "My Profile";
        $plans = Plans::all();
        return view('dashboard.profile',['title' => $title,'plans'=>$plans]);
    }

    public function bankPage(Request $request){
        $title = "Bank Details";
        $banks = Banks::all();
        return view('dashboard.bank',['title' => $title,'banks' => $banks]);
    }

    public function upgradePage(Request $request){
        $title = "Upgrade Plan";
        $plans = Plans::all();
        return view('dashboard.upgrade',['title' => $title,'plans'=>$plans]);
    }

    public function pairUser(Request $request){
        $title = "Pair User";
        $users = User::where('active','1')->doesntHave('pair_batch')->get();
        return view('dashboard.pair_users',['title' => $title,'users' => $users]);
    }

    public function getUsersPage(Request $request){
        $title = "Users";
        $users = User::all();
        return view('dashboard.users',['users'=>$users,'title' => $title]);
    }

    public function showStats(Request $request){
        $title = "Statistics";
        $plans = Plans::all();
        $profile_complete = User::whereHas('usermetas', function($query){
            $query->where('bank_id','!=',null)->where('account_number','!=',null);
        })->count();
        $profile_not_complete = User::whereHas('usermetas', function($query){
            $query->where('bank_id',null)->where('account_number',null);
        })->count();
        $total_users = User::all()->count();
        $waiting_users = User::where('active','1')->doesntHave('pair_batch')->orWhereHas('pair_batch', function($query){
            $query->where('active','0');
        })->count();
        $paired = User::where('active','1')->whereHas('pair_batch', function($query){
            $query->where('active','1');
        })->count();

        $current_paying = User::where('active','0')->whereHas('pair_users', function($query){
            $query->where('paid','0')->whereHas('pair_batch', function($query1){
                $query1->where('active','1');
            });
        })->count();

        $waiting_pay_for_bronze = QueuePair::where('status','0')->whereHas('user', function($query){
            $query->whereHas('usermetas', function($query1) {
                $query1->where('bank_id','!=',null)->where('account_number','!=',null)->where('plan_id','1');
            });
        })->count();

        $waiting_pay_for_silver = QueuePair::where('status','0')->whereHas('user', function($query){
            $query->whereHas('usermetas', function($query1) {
                $query1->where('bank_id','!=',null)->where('account_number','!=',null)->where('plan_id','2');
            });
        })->count();



        return view('dashboard.statistics',['title' => $title,'plans' => $plans,'total'=>$total_users,'profile_complete' =>  $profile_complete,
        'profile_not_complete' => $profile_not_complete,'waiting_user'=>$waiting_users,'paired' => $paired,'current_paying' => $current_paying,'bronze_pay' =>$waiting_pay_for_bronze,
        'silver_pay' => $waiting_pay_for_silver]);

    }

}
