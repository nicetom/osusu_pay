<?php

namespace App\Http\Controllers\User;
use Validator;
use App\Models\User;
use App\Models\UserMeta;
use App\Models\Pair_Batch;
use App\Models\Pair_Batch_Backup;
use App\Models\Pair_Users;
use App\Models\QueuePair;
use App\Models\QueueMatch;
use App\Models\Pair_User_Backup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Controllers\LoadController as First;
use Session;
class LoadController extends Controller
{

    private $first;

    public function __construct(First $first){
        $this->first = $first;
    }

    public function validateData(array $data){
        return Validator::make($data,[
            'bank_name' => ['required','exists:banks,id'],
            'account_number' => ['required','digits:10']
        ]);
    }

    public function addBankData(Request $request){
        $validation = $this->validateData($request->except('_token'));

        if($validation->fails()){
            return redirect()->back()->withErrors($validation->getMessageBag())->withInput();
        }else{
            $user = UserMeta::find(Auth::guard('web')->user()->usermetas->id);
            $user->bank_id = $request['bank_name'];
            $user->account_number = $request['account_number'];
            $user->save();
            $this->first->newPairing($request);

            Session::put('green',1);
            return redirect()->back()->withErrors('Bank Details have been successfully uploaded. You\'ll be paired in the next 24-48 Hours');

        }
    }

    public function validateProfile(array $data){
        return Validator::make($data,[
            'firstname' => ['required','string'],
            'lastname' => ['required','string'],
            'plan' => ['required','numeric','exists:plans,id'],
            'phone' => ['required','numeric']
        ]);
    }

    public function replaceUsers(Request $request){
        $data = $request->except('_token');

        $user = User::find($data['free']);

            if(($user->pair_batch->count() == 0) & ($user->pair_users->count() == 0)){

                $pair = Pair_Users::find($data['payer_id']);
                $pair->reciever_id = $user->id;
                $pair->save();

                $pair_backup = Pair_User_Backup::find($data['payer_id']);
                $pair_backup->reciever_id = $user->id;
                $pair_backup->save();
                Session::put('green',1);
                return redirect()->back()->withErrors('User Replaced Successfully');

            }else{

                Session::put('red',1);
                return redirect()->back()->withErrors('User Selected just got allocated to be paid to get paid');
            }
    }

    public function updateProfile(Request $request){
        $validation = $this->validateProfile($request->except('_token'));
        if($validation->fails()){
            Session::put('red',1);
            return redirect()->back()->withErrors($validation->getMessageBag())->withInput();
        }else{
            $user = User::find(Auth::guard('web')->user()->id);
            $user->firstname = $request['firstname'];
            $user->lastname = $request['lastname'];
            $user->phone_number = $request['phone'];
            $user->save();
                if($user->usermetas->plan_id != $request['plan']){
                    UserMeta::where('user_id',$user->id)->update(['plan_id' => $request['plan']]);
                }
            Session::put('green',1);
            return redirect()->back()->withErrors('Account Profile details have been successfully uploaded.');
        }
    }

    public function upgradePlan(Request $request){
            $user = UserMeta::find(Auth::guard('web')->user()->usermetas->id);
            $user->plan_id = $request['new_plan'];
            $user->save();
            $main_user = User::find(Auth::guard('web')->user()->id);
            $main_user->active = '0';
            $main_user->save();
            Session::put('green',1);
            return redirect()->back()->withErrors('Plan has been upgraded.');
    }

    public function getUsersForPlan(Request $request,$user_id){
        $user = User::find($user_id);
        $plan = $user->usermetas->plan_id;

        $users_to_return = QueuePair::where('status','0')->whereHas('user', function($query)use($plan){
            $query->whereHas('usermetas', function($query1) use($plan){
                $query1->where('bank_id','!=',null)->where('account_number','!=',null)->where('plan_id',$plan);
            });
        })->get();

       
        $data = array();
        foreach($users_to_return as $pair){

            $data[$pair->user->id] = $pair->user->firstname." ".$pair->user->lastname." - ".$pair->user->usermetas->plan->name." ".$pair->user->usermetas->plan->amount;
        }

        return response()->json(['users' => $data])->header('Content-type', "application/json");
    }

    public function pairUsers(Request $request){
             $new_batch = new Pair_Batch;
        $new_batch->source_id = $request['source'];
        $new_batch->active = '1';
        $new_batch->save();

        Pair_Batch_Backup::create([
            'source_id' => $request['source'],
            'active' => '1',
        ]);

        foreach($request['users'] as $user){
            $pair_users = new Pair_Users;
            $pair_users->batch_id = $new_batch->id;
            $pair_users->reciever_id = $user;
            $pair_users->paid = '0';
            $pair_users->plan_id = User::find($user)->usermetas->plan->id;
            $pair_users->save();

            Pair_User_Backup::create([
                'batch_id' => $new_batch->id,
                'reciever_id' => $user,
                'paid' => '0',
                'plan_id' => User::find($user)->usermetas->plan->id,
            ]);
        }

        Session::put('green',1);
        return redirect()->back()->withErrors('Users have been paired successfully');

    }

    public function confirmPayment(Request $request){
        $pp = Pair_Users::find($request['pair_id']);
        $pp->paid =  '1';
        $pp->save();
        QueueMatch::create([
            'user_id' => $pp->reciever_id,
        ]);
        Pair_User_Backup::where('id',$request['pair_id'])->update(['paid' => '1']);

        $user = User::find($pp->reciever_id);
        $user->active = '1';
        $user->save();

        $bb = $pp->batch_id;
        $p2 = Pair_Users::where('batch_id',$bb)->get();
        $p2_count = $p2->count();
        $check_count = 0;
            foreach($p2 as $p){
                if($p->paid == '1'){
                    $check_count = $check_count + 1;
                }
            }

        if($check_count == $p2_count){
            $p1 = Pair_Batch::find($bb);
            $p1->active = '0';
            $p1->save();
            $us = User::find($p1->source_id);
            $us->active = '2';
            $us->save();

        }
        $this->first->newPairing($request);
        Session::put('green',1);
        return redirect()->back()->withErrors('Successful Confirmation');
    }

    public function recycleUser(Request $request){
        $user = User::find(Auth::guard('web')->user()->id);
        $user->active = '0';
        $user->save();
        QueuePair::create([
            'user_id' => $user->id,
        ]);
        return redirect()->back()->withErrors('You have been Recycled on the same plan.');
    }

    public function banUser(Request $request,$user_id){
        $user = User::find($user_id);
        if(($user->pair_batch->count() == 0) & ($user->pair_users->count() == 0)){
            $user->active = '3';
            $user->save();

            Session::put('green',1);
            return redirect()->back()->withErrors('User has been banned Successfully.');
        }else{
        Session::put('red',1);
        return redirect()->back()->withErrors('User cannot be banned at this time because he is expecing payment or needs to pay someone.');
        }

    }
}
