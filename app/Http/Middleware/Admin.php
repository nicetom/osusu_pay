<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard('web')->user()->role != '0'){
            redirect()->route('login_page')->withErrors(["register_success" => "You're not authenticated. Please Login as an Administrator."]);
        }
        return $next($request);
    }
}
