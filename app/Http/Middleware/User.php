<?php

namespace App\Http\Middleware;
use Auth;
use Session;
use Closure;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::guard('web')->check()){
            Session::put('red','1');
            return redirect()->route('login_page')->withErrors(['register_success'=>"You're not Authenticated. Please Log in."]);
        }
        return $next($request);
    }
}
