<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pair_User_Backup extends Model
{
    protected $table = "paired_users_backup";

    protected $fillable = [
        'batch_id','reciever_id','paid','plan_id'
    ];

    public function pair_batch(){
        return $this->hasOne(Pair_Batch::class,'id','batch_id');
    }

    public function user(){
        return $this->hasOne(User::class,'id','reciever_id');
    }
}
