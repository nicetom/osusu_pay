<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class UserMeta extends Model
{
    protected $table = 'users_meta';

    protected $fillable = ['plan_id','account_number','bank_name','user_id'];

    public $timestamps = false;

    public function plan(){
        return $this->hasOne(Plans::class,'id','plan_id');
    }

    public function bank(){
        return $this->hasOne(Banks::class,'id','bank_id');
    }
}
