<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QueuePair extends Model
{
    protected $table = "queue_pairs";

    protected $fillable = [
        'user_id','status'
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
}
