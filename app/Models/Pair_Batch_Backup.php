<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pair_Batch_Backup extends Model
{
    protected $table = "pair_batch_backup";

    protected $fillable = [
        'source_id','active'
    ];

    public function pair_users(){
        return $this->hasMany(Pair_Users::class,'batch_id','id');
    }

    public function users(){
        return $this->hasOne(User::class,'id','source_id');
    }
}
