<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plans extends Model
{
    protected $table = 'plans';

    public function usermeta(){
        return $this->belongsTo(UserMeta::class);
    }

    public function usermetas(){
        return $this->hasMany(UserMeta::class,'plan_id','id');
    }


}
