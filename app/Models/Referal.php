<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Referal extends Model
{
    protected $table = "referrals";
    public $timestamps = false;

    protected $fillable = [
        'user_id','tag','referral_clicks','referral_registered'
    ];
}
