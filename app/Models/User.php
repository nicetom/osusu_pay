<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname' ,'email', 'password', 'phone_number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function usermetas(){
        return $this->hasOne(UserMeta::class,'user_id','id');
    }

    public function referrals(){
        return $this->hasOne(Referal::class,'user_id','id');
    }

    public function pair_batch(){
        return $this->hasMany(Pair_Batch::class,'source_id','id');
    }


    public function pair_users(){
        return $this->hasMany(Pair_Users::class,'reciever_id','id');
    }

    public function onepair(){
        return $this->hasOne(Pair_Users::class,'reciever_id','id');
    }

    public function pair_batch_backup(){
        return $this->hasMany(Pair_Batch_Backup::class,'source_id','id');
    }

     public function pair_users_backup(){
        return $this->hasMany(Pair_User_Backup::class,'reciever_id','id');
    }

    public function queue_pair(){
        return $this->hasMany(QueuePair::class,'user_id','id');
    }

    public function queue_match(){
        return $this->hasMany(QueueMatch::class,'user_id','id');
    }
}
