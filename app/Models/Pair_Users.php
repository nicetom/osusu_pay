<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pair_Users extends Model
{
    protected $table = "paired_users";

    protected $fillable = [
        'batch_id','reciever_id','paid','plan_id'
    ];

    public function pair_batch(){
        return $this->belongsTo(Pair_Batch::class,'batch_id','id');
    }

    public function user(){
        return $this->hasOne(User::class,'id','reciever_id');
    }


}
