let xmlhttp = null;
if (window.XMLHttpRequest) {
    // code for modern browsers
    xmlhttp = new XMLHttpRequest();
 } else {
    // code for old IE browsers
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
}


function get_LGAs_by_state(state_id) {
  xmlhttp.open('GET','banks', false);
  xmlhttp.send();
  if (xmlhttp.status == 200) {
        return xmlhttp.responseText;
   }
   else return ''
}


function processData(json_data_str, select_elem_id) {

  //small function to generate the html select option
  function generate_option(lga_data) {
    let option = document.createElement('option');
    for(let key in lga_data) {
      option.setAttribute('value', key);
      option.innerHTML = lga_data[key];
    }
    return option;
  }


  try {
    let lga_data = JSON.parse(json_data_str)
    let lga_array = lga_data.lga;   //get the array of LGAs

    let parentSelect = document.getElementById(select_elem_id);
    parentSelect.innerHTML = '';
    parentSelect.appendChild(generate_option({'-':'Select'}));
    //loop through each lga
    for(let i = 0; i < lga_array.length; i++) {
      // attach generated option to the parent element
      parentSelect.appendChild(generate_option(lga_array[i]));
    }
  }
  catch(err) {
    console.log(err.message);
  }
}
