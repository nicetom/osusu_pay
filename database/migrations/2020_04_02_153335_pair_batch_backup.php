<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PairBatchBackup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pair_batch_backup', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('source_id')->unsigned();
            $table->foreign('source_id')->references('id')->on('users')->onDelete('cascade');
            $table->enum('active',['0','1'])->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pair_batch_backup');
    }
}
