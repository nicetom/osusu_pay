<?php

use Illuminate\Database\Seeder;

class planSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = array('Bronze' => '2500','Silver' => '5000');

        foreach($plans as $plan => $amount){
            DB::table('plans')->insert([
                'name' => $plan,
                'amount' => $amount
                ]);
        }
    }
}
