<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Plans;
use App\Models\Banks;
use Illuminate\Support\Facades\Hash;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $plans = Plans::all();
        $banks = Banks::all();
        $plan_array = [];
        $bank_array = [];
        $active_array = ['0','1'];

        foreach($plans as $plan){
            array_push($plan_array,$plan->id);
        }

        foreach($banks as $bank){
            array_push($bank_array,$bank->id);
        }

        for($i = 0; $i < 20; $i++){
            $user = User::create([
                'firstname' => $faker->firstName,
                'lastname' => $faker->lastName,
                'email' => $faker->unique()->safeEmail,
                'phone_number' => $faker->e164PhoneNumber,
                'role' => '1',
                'active' => $faker->randomElement($active_array),
                'password' => Hash::make('password'),
            ]);

            $user->referrals()->create([
                'user_id' => $user->id,
                'tag' => md5($user->id)
            ]);

            $user->usermetas()->create([
                'user_id' => $user->id,
                'plan_id' => $faker->randomElement($plan_array),
                'bank_id' => $faker->randomElement($bank_array),
                'account_number' => $faker->randomNumber(8),

            ]);

                if($user->active == '0'){
                    $user->queue_pair()->create([
                        'user_id' => $user->id,
                    ]);
                }else if($user->active == '1'){
                    $user->queue_match()->create([
                        'user_id' => $user->id,
                    ]);
                }
        }
    }
}
