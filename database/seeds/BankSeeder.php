<?php

use Illuminate\Database\Seeder;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banks = array("Access Bank" ,"Diamond Bank" ,"Fidelity Bank" ,"First City Monument Bank" ,"First Bank of Nigeria" ,"Guaranty Trust Bank" ,"Union Bank","United Bank for Africa (UBA)" ,"Zenith Bank" ,"Citi Bank Nigeria ","EcoBank Nigeria" ,"Heritage Bank","Keystone Bank" ,"Polaris Bank","Stanbic IBTC Bank" ,"Standard Chartered Bank","Sterling Bank" ,"Unity Bank" ,"Wema Bank" ,"SunTrust Bank" ,"Providus Bank" ,"Jaiz Bank" ,"Coronation Merchant Bank","FBNQuest Merchant Bank","FSDH Merchant Bank","Rand Merchant Bank","Nova Merchant Bank");
        sort($banks);
        foreach($banks as $bank ){
            DB::table('banks')->insert([
                'name' => $bank
                ]);
        }
    }
}
