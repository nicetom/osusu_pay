$(document).ready(function() {
    'use strict';
    var base_url = $('meta[name="base_url"]').attr('content');

       $("#user").change(function(e){
           e.preventDefault();
            var user_id = $("select[name='source']").val();


           $.ajax({
               url: base_url + '/dashboard/admin/plans/'+user_id,
               type:'GET',
               dataType: "json",
               cache: false,

           }).done(function(data){
               $('#parent').empty();
               
            jQuery.each(data.users, function(i, val) {
                var option = $('<option></option>');
                $(option).attr('value',i).text(val);
                $(option).appendTo('#parent');
            });

              });

       });

});
