$(document).ready(function() {
    'use strict';
    var base_url = $('meta[name="base_url"]').attr('content');

       $("#reciever").change(function(e){
           e.preventDefault();

            var source = $("select[name='source_id']").val();

           $.ajax({
               url: base_url + "/payers/"+source,
               type:'GET',
               dataType: "json",
               cache: false,

           }).done(function(data){
               $('#payer').empty();
            getFreeUsers(source);
            jQuery.each(data, function(i, val) {
                var option = $('<option></option>');
                $(option).attr('value',i).text(val);
                $(option).appendTo('#payer');
            });

              });

       });

       function getFreeUsers(source_id){
        $.ajax({
            url: base_url + "/users/"+source_id,
            type:'GET',
            dataType: "json",
            cache: false,

        }).done(function(data){
            $('#free_users').empty();


         jQuery.each(data, function(i, val) {
             var option = $('<option></option>');
             $(option).attr('value',i).text(val);
             $(option).appendTo('#free_users');
         });

    });
       }



});
