<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@home' );

Route::get('signin','PageController@loginPage')->name('login_page');

Route::get('signup','PageController@registerPage')->name('register_page');

Route::get('referral/{tag}','PageController@referralLink')->name('referral');

Route::get('automatic_pairing','LoadController@newPairing')->name('automatic_pairing');

Route::get('forget_password','PageController@forgetPassword')->name('forget');

Route::post('forget_password','LoadController@forgetPassword')->name('forget_action');

Route::get('change_password/{token}','PageController@changePassword')->name('change_password');

Route::get('queue','LoadController@populateQueue');

Route::post('change_password','LoadController@changePassword')->name('change_password_action');


Route::group(['prefix' => 'auth'], function(){
    Route::post('register','LoadController@createUser')->name('registerUser');

    Route::post('login','Auth\LoginController@authenticate')->name('loginUser');

    Route::get('logout','Auth\LoginController@logout')->name('logout');
});

Route::get('payers/{source_id}','LoadController@getPayers');
Route::get('users/{plan_id}','LoadController@getUnpairedUsers');

Route::group(['prefix' => 'dashboard','namespace' => 'User','middleware' => 'user'], function () {
    Route::get('home','PageController@indexPage')->name('home');

    Route::get('profile','PageController@profilePage')->name('profile');

    Route::post('recycle','LoadController@recycleUser')->name('recycle-user');

    Route::get('bank_details','PageController@bankPage')->name('bank');

    Route::post('update_profile','LoadController@updateProfile')->name('update_profile');

    Route::post('update_bank_details','LoadController@addBankData')->name('update_bank_details');

    Route::get('upgrade_page','PageController@upgradePage')->name('upgradePage');

    Route::post('upgrade_plan','LoadController@upgradePlan')->name('upgrade_plan');

    Route::post('confirm_payment','LoadController@confirmPayment')->name('confirm');

    Route::get('recycle_plan','LoadController@recyclePlan')->name('recycle');

    Route::group(['prefix' => 'admin','middleware' => 'admin'], function () {
        Route::get('pair_users','PageController@PairUser')->name('pair');

        Route::get('replace_users','PageController@replaceUsers')->name('replace_user');

        Route::post('replace','LoadController@replaceUsers')->name('replace_user_action');

        Route::get('users','PageController@getUsersPage')->name('users');

        Route::get('plans/{user_id}','LoadController@getUsersForPlan')->name('users_plans');

        Route::post('pair_users','LoadController@pairUsers')->name('pair_action');

        Route::get('statistics','PageController@showStats')->name('statistics');

        Route::post('ban/{user_id}','LoadController@banUser')->name('ban_user');
    });

});
